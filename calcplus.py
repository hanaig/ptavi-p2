import calcoohija

if __name__ == "__main__":
    try:
        archivo = open("fichero.csv")
        for linea in archivo:
            datos = linea.split(',')
            result = int(datos[1])
            if datos[0] == "suma":

                for op2 in datos[2:]:
                    result = calcoohija.CalculadoraHija(result,
                                                        int(op2)).plus()
                print(result)
            elif datos[0] == "resta":

                for op2 in datos[2:]:
                    result = calcoohija.CalculadoraHija(result,
                                                        int(op2)).minus()
                print(result)
            elif datos[0] == "multiplicacion":

                for op2 in datos[2:]:
                    result = calcoohija.CalculadoraHija(result,
                                                        int(op2)).multiplicar()
                print(result)
            elif datos[0] == "division":

                for op2 in datos[2:]:
                    result = calcoohija.CalculadoraHija(result,
                                                        int(op2)).dividir()
                print(result)
            else:
                print("Operación sólo puede ser sumar, "
                      "restar, dividir o multiplicar.")
    except ValueError:
        print("Error: Non numerical parameters")

        archivo.close()
