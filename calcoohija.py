import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):

    def __init__(self, op1, op2):
        super().__init__(op1, op2)
        self.op1 = op1
        self.op2 = op2

    def multiplicar(self):

        return self.op1 * self.op2

    def dividir(self):
        try:
            return self.op1 / self.op2
        except ZeroDivisionError:
            print("Division by Zero is not allowed")


if __name__ == "__main__":
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    if sys.argv[2] == "suma":
        result = CalculadoraHija(operando1, operando2).plus()
    elif sys.argv[2] == "resta":
        result = CalculadoraHija(operando1, operando2).minus()
    elif sys.argv[2] == "multiplicacion":
        result = CalculadoraHija(operando1, operando2).multiplicar()
    elif sys.argv[2] == "division":
        result = CalculadoraHija(operando1, operando2).dividir()
    else:
        sys.exit('Operación sólo puede ser sumar, '
                 'restar, dividir o multiplicar.')

    print(result)
